require 'minitest/autorun'
require 'webmock/minitest'

require_relative '../lib/WiproCrawler/parser'

describe 'parser test case' do
  it 'does not return known_links before parsing' do
    parser = Parser.new

    parser.known_links.size.must_equal 0
  end

  it 'retrieves 1 link' do
    stub_request(:any, 'http://www.wiprodigital.com')
      .to_return(body: File.new('test/fixtures/files/onelink.html'),
                 status: 200)

    parser = Parser.new
    parser.parse('http://www.wiprodigital.com')

    parser.known_links.size.must_equal 1
  end

  it 'obeys the maximum recusion depth' do
    stub_request(:any, 'http://www.wiprodigital.com')
      .to_return(body: File.new('test/fixtures/files/onelink.html'),
                 status: 200)
    stub_request(:get, 'http://www.wiprodigital.com/localfile.html')
      .to_return(body: File.new('test/fixtures/files/localfile.html'),
                 status: 200)

    parser = Parser.new
    parser.parse('http://www.wiprodigital.com', 2)
    parser.known_links.size.must_equal 2
  end

  it 'does not follow duplicate known_links' do
    stub_request(:any, 'http://www.wiprodigital.com')
      .to_return(body: File.new('test/fixtures/files/duplicatelinks.html'),
                 status: 200)

    parser = Parser.new
    parser.parse('http://www.wiprodigital.com')

    parser.known_links.size.must_equal 1 # the original URL, plus the link found in the HTML file
  end

  it 'only follows known_links within the current site' do
    stub_request(:any, 'http://www.wiprodigital.com')
      .to_return(body: File.new('test/fixtures/files/localandremotelinks.html'),
                 status: 200)

    parser = Parser.new
    parser.parse('http://www.wiprodigital.com')

    parser.known_links.size.must_equal 2 # we record the external link, but do not parse it or follow
  end

  it 'handles relative and absolute URLs' do
    stub_request(:any, 'http://www.wiprodigital.com')
      .to_return(body: File.new('test/fixtures/files/relateiveandabsolutelinks.html'),
                 status: 200)
    stub_request(:any, 'http://www.wiprodigital.com/relative.html')
      .to_return(body: File.new('test/fixtures/files/relative.html'),
                 status: 200)
    stub_request(:any, 'http://www.wiprodigital.com/localfile.html')
      .to_return(body: File.new('test/fixtures/files/localfile.html'),
                 status: 200)
    stub_request(:any, 'http://www.wiprodigital.com/onelink.html')
      .to_return(body: File.new('test/fixtures/files/onelink.html'),
                 status: 200)
    parser = Parser.new
    parser.parse('http://www.wiprodigital.com', 2)
    parser.known_links.size.must_equal 3 # 2 in the file we're starting with, and one in the file in the relative URL
  end

  it 'must generate a sitemap' do
    stub_request(:any, 'http://www.wiprodigital.com')
      .to_return(body: File.new('test/fixtures/files/localandremotelinks.html'),
                 status: 200)

    parser = Parser.new
    parser.parse('http://www.wiprodigital.com')

    parser.sitemap.children.size.must_equal 2
  end
end
