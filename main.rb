require_relative 'lib/WiproCrawler/parser'

options = {}

options[:max_depth] = ARGV[1].to_i

options[:url] = ARGV[0]

if !(options[:url].nil? || options[:max_depth].nil?)
  parser = Parser.new

  parser.parse(options[:url], options[:max_depth])

  parser.sitemap.print_tree

else
  puts 'usage: main.rb URL_TO_PARSE MAX-DEPTH'
  exit
end
