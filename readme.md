# Wipro Web Crawler

# How to build and run your solution
[Install Ruby](https://www.ruby-lang.org/en/documentation/installation/)
Open a command terminal, change to the root of this project.
Run `bundle install` to install dependencies.
Run `rake test` to execute the test suite.
Run `ruby main.rb http://www.wiprodigital.com/  3` to crawl the Wipro Digital website. 

# Reasoning and describe any trade offs

I used Ruby Gems for parsing web pages and managing the "tree". This simplified the solution, and made it more readable.

I used Minitest to create human-readable tests, and used TDD to drive the design.

My IDE is currently a bit broken, so I've not spent a lot of time refactoring (see below).

In order to keep the run-time reasonable, I've added an option to limit the depth of recursion; 
this is passed in on the command line as a parameter.

While not going overboard on the hygiene factors, I've set up a [CI pipeline](https://gitlab.com/neville.kuyt/WiproCrawler/pipelines) on Gitlab to run the test suite on commit/push.
# Explanation of what could be done with more time
This version does not include images or other linked resources as requested in the instructions - it only honours the anchor tag.

I haven't implemented logic to standardize the way URLs are stored, so it's returning duplicates for    `www.domain.com` and `www.domain.com/`.

I currently include anchor tags for in-page links (`www.domain.com\index.html#about`). Not sure if this is a bug or a feature.

The code is readable, but a little messy. My IDE's refactoring support is broken, and I spent 2 hours just dealing with different Ruby versions, so didn't pursue that. I would like to reduce the length and complexity of the `parse` method, probably by extracting methods for initialization and link validation.

For scalability, an async queue system would be nice - this would allow the application to run concurrent crawls, and remain responsive to user interaction.
## Instructions from Buildit

### What we are looking for

This exercise is to examine your technical knowledge, reasoning and engineering principles. There are no tricks or hidden agendas.

We are looking for a demonstration of your experience and skill using current software development technologies and methods. Please make sure your code is clear and demonstrates good practices.

Your solution will form the basis for discussion in subsequent interviews.
### What you need to do

Please write a simple web crawler in a language of your choice in a couple of hours – please don’t spend much more than that.

The crawler should be limited to one domain. Given a starting URL – say http://wiprodigital.com - it should visit all pages within the domain, but not follow the links to external sites such as Google or Twitter.

The output should be a simple structured site map (this does not need to be a traditional XML sitemap - just some sort of output to reflect what your crawler has discovered), showing links to other pages under the same domain, links to external URLs and links to static content such as images for each respective page.

Please provide a README.md file that explains how to run / build your solution. Also, detail anything further that you would like to achieve with more time.

Once done, please make your solution available on Github and forward the link. Where possible please include your commit history to give visibility of your thinking and progress.
### What you need to share with us

    Working crawler as per requirements above
    A README.md explaining
        How to build and run your solution
        Reasoning and describe any trade offs
        Explanation of what could be done with more time
    Project builds / runs / tests as per instruction

