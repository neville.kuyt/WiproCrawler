
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = 'Wipro Crawler'
  spec.version       = '0.1'
  spec.authors       = ['Neville Kuyt']
  spec.email         = ['neville.kuyt@gmail.com']
  spec.summary       = 'Crawler to satisfy Wipro Coding test'
  spec.description   =
    spec.homepage = 'https://gitlab.com/neville.kuyt/WiproCrawler'
  spec.license       = 'MIT'

  spec.files         = ['lib/NAME.rb']
  spec.executables   = ['bin/NAME']
  spec.test_files    = ['tests/test_NAME.rb']
  spec.require_paths = ['lib']
end
