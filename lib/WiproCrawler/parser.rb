require 'nokogiri'
require 'open-uri'
require 'tree'

# A simple HTML parser.
class Parser
  def initialize
    @page = nil
    @known_links = []
    @host_to_parse = ''
    @protocol = 'http://' # TODO: extract from initial URL
  end

  def sitemap
    @sitemap
  end

  def parse(url, max_depth = 1, current_depth = 0, current_node = nil)
    return false if current_depth >= max_depth

    if 0 == current_depth
      @host_to_parse = URI.parse(url).host.downcase
      @sitemap = Tree::TreeNode.new(@host_to_parse, "Root Content")
      current_node = @sitemap
    end

    @page = Nokogiri::HTML(open(url))
    known_links.each do |link|
      link_host = URI.parse(link).host

      if link_host.nil? #relative URL
        link = '/' + link unless link.start_with?('/')
        link = @protocol + @host_to_parse + link
      end
      if current_node[link].nil? #don't visit known_links we've already done.
        new_node = current_node << Tree::TreeNode.new(link)

        if link_host.nil? || link_host.downcase == @host_to_parse
          parse(link, max_depth, current_depth + 1, new_node)
        end
      end
    end
  end

  def known_links
    #Todo: guard clause is not very elegant..
    return @known_links if @page.nil?

    @known_links += @page.xpath('//a').map { |element| element['href'] }.compact
    @known_links.uniq
  end
end
